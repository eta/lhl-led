//! Blinks an LED
//!
//! This assumes that a LED is connected to pc13 as is the case on the blue pill board.
//!
//! Note: Without additional hardware, PC13 should not be used to drive an LED, see page 5.1.2 of
//! the reference manual for an explanation. This is not an issue on the blue pill.

#![deny(unsafe_code)]
#![no_std]
#![no_main]

use cortex_m::delay::Delay;
use cortex_m::iprintln;
use cortex_m::peripheral::syst::SystClkSource;
use panic_semihosting as _;

use nb::block;

use core::fmt::Write;
use cortex_m_rt::entry;
use cortex_m_semihosting::hio;
use stm32f1xx_hal::gpio::PinState;
use stm32f1xx_hal::{pac, prelude::*};

#[entry]
fn main() -> ! {
    // Get access to the core peripherals from the cortex-m crate
    let mut cp = cortex_m::Peripherals::take().unwrap();
    // Get access to the device specific peripherals from the peripheral access crate
    let dp = pac::Peripherals::take().unwrap();

    // Take ownership over the raw flash and rcc devices and convert them into the corresponding
    // HAL structs
    let mut flash = dp.FLASH.constrain();
    let mut rcc = dp.RCC.constrain();

    // Freeze the configuration of all the clocks in the system and store the frozen frequencies in
    // `clocks`
    let clocks = rcc.cfgr.freeze(&mut flash.acr);

    // Acquire the GPIOC peripheral
    let mut gpioa = dp.GPIOA.split();
    let mut gpioc = dp.GPIOC.split();

    // Configure gpio C pin 13 as a push-pull output. The `crh` register is passed to the function
    // in order to configure the port. For pins 0-7, crl should be passed instead.
    let mut led = gpioc.pc9.into_push_pull_output(&mut gpioc.crh);
    let mut delay = Delay::with_source(cp.SYST, clocks.sysclk().to_Hz(), SystClkSource::Core);

    let mut addr_a = gpioc.pc6.into_push_pull_output(&mut gpioc.crl);
    let mut oe = gpioc.pc7.into_push_pull_output(&mut gpioc.crl);
    let mut latch = gpioc.pc8.into_push_pull_output(&mut gpioc.crh);
    let mut clk = gpioa.pa9.into_push_pull_output(&mut gpioa.crh);
    let mut red_1 = gpioa.pa8.into_push_pull_output(&mut gpioa.crh);

    let mut stdout = hio::hstdout().unwrap();
    writeln!(stdout, "it's go time! (in like a second)").unwrap();
    delay.delay_ms(1000);
    loop {
        writeln!(stdout, "clocking out a bunch of red pixels?").unwrap();
        clk.set_low();
        for i in 0..16 {
            // send thing on SDI
            red_1.set_state(if i % 2 == 0 {
                PinState::High
            } else {
                PinState::Low
            });
            // wait
            delay.delay_us(1);
            clk.set_high();
            delay.delay_us(1);
            if i == 15 {
                latch.set_low();
            }
            clk.set_low();
            if i == 14 {
                latch.set_high();
            }
        }
        writeln!(stdout, "sending vsync??").unwrap();
        delay.delay_us(1);
        latch.set_high();
        // "LE is high for 2 DCLKs rising edge"
        for i in 0..2 {
            delay.delay_us(1);
            clk.set_low();
            delay.delay_us(1);
            clk.set_high();
        }
        latch.set_low();
        oe.set_low();
        writeln!(stdout, "did it do anything??").unwrap();
    }
}
